## - Bruxo -
## Currently learning DevOps - Interested in reverse operating system engineering - Focusing on Python and Bash scripting - Pursuing LPIC certification - Passionate about low-level programming - Enthusiastic about open-source projects, privacy, and decentralized programs

I am Bruxo, a dedicated learner in the field of DevOps. My current focus is on expanding my knowledge and skills in reverse operating system engineering. I am particularly interested in understanding and manipulating operating systems at a fundamental level.

My primary programming languages of choice are Python and Bash, and I am actively working towards obtaining LPIC certification to further enhance my expertise.

Low-level programming fascinates me as it allows me to explore the inner workings of computer systems and optimize their performance.

I have a deep appreciation for open-source projects, as they embody the collaborative spirit of the developer community. Additionally, I am a strong advocate for privacy and decentralized programs, recognizing their significance in safeguarding user data and promoting user autonomy.

I am eager to contribute to meaningful projects and collaborate with like-minded individuals who share my passion for technology and its potential to shape a better future.
